import openpyxl as opx

CARPETA = "C:\\Users\\Usuario\\Documents\\Frontend Intermedio\\ATENEA Documentos Estudiantes\\Documentos\\"
ARCHIVOS = [
    "Listado Definitivo- Cursos, Profesores, Tutores, Monitores .xlsx",
    "Respuesta Inscritos ATENEA.xlsx",
]
CURSO = "FrontEnd Intermedio Grupo 1"


def obtener_cedulas():
    book = opx.load_workbook(
        CARPETA + ARCHIVOS[0],
        data_only=True,
    )
    hoja = book[CURSO]

    # Modificar este rango identificando el rango en que se encuentran la mayoría de datos
    # En este rango solo se encuentran los identificadores(CC)
    celdas = hoja["D13":"D136"]
    cedulas = []
    for fila in celdas:
        for celda in fila:
            cedulas.append(int(celda.value))
    # Para probar
    # print("########## esta en la lista?: ", ########## in cedulas)
    return cedulas


def obtener_links():
    book = opx.load_workbook(
        ARCHIVOS[1],
        data_only=True,
    )
    hoja = book["Base Aprobados"]

    # Cambiar este rango también, poner celdas desde cédula hasta último link
    datos = hoja["E3445":"AA4671"]

    dic_estudiantes = {}
    for fila in datos:
        estudiante = [celda.value for celda in fila]
        dic_estudiantes[int(estudiante[0])] = [
            estudiante[14],
            estudiante[17],
            estudiante[19],
            estudiante[21],
            estudiante[22],
        ]
    return dic_estudiantes
