import wget
import os
import lector_datos as lector

# Modificar ruta de descargas a preferencia
RUTA_DESCARGAS = "C:\\Users\\Usuario\\Downloads\\Documentos Estudiantes\\"
NOMBRES = [
    "",
    "_RESIDENCIA",
    "_DIPLOMA",
    "_MATERNIDAD",
    "_MINORIA",
]
EXT = ".pdf"

cedulas = lector.obtener_cedulas()
dic = lector.obtener_links()

for i in range(0, len(cedulas)):
    key = cedulas[i]
    os.system("cls")
    print(i + 1, "de", len(cedulas), "\nDescargando archivos de:", str(key))
    links = dic[key]
    try:
        os.mkdir(RUTA_DESCARGAS + str(key))
    except OSError:
        print("La creación del directorio falló")
    else:
        for i in range(0, len(links)):
            if links[i] != None:
                wget.download(
                    links[i],
                    RUTA_DESCARGAS + str(key) + "\\" + str(key) + NOMBRES[i] + EXT,
                )
os.system("cls")
print("Hecho :D")
