# Requisitos

- Python
- pip
- openpyxl
- wget

En caso de no tener pip, openpyxl o wget, usar

```
pip install 'nombre del paquete'
```

# Pre ejecución

Para ejecutar el programa correctamente se deben descargar los archivos **_Listado Definitivo- Cursos, Profesores, Tutores, Monitores .xlsx_** y **_Respuesta Inscritos ATENEA.xlsx_** con el mismo nombre en la misma carpeta en donde se encuentre el programa.

Ahora, se debe modificar el archivo **lector_datos.py** para obtener los datos del curso que se espera

1. Modificar la variable **CURSO** con el nombre que tenga en excel.

2. Se deben identificar dos rangos(visualmente desde los excel) para ingresarlos

   - El primer rango es el de las cedulas de los estudiantes de nuestro curso, tomado del primer archivo y se modifica en la línea 19.

   - El segundo rango es el correspondiente a los enlaces de los archivos a descargar, tomado del segundo archivo y va desde la columna de cedula hasta la última columna que tenga links. Dicho rango se modifica en la línea 37.

En este punto. Se debe modificar en donde se espera que se descarguen los archivos modificando el valor de la variable **RUTA_DESCARGAS** en el archivo **receptor.py**

# Final

Una vez hecho esto ya se podría ejecutar, tener en cuenta que en la carpeta especificada anteriormente se creará una carpeta por cada estudiante
